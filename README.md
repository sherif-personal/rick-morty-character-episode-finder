# Rick and Morty Character Episodes finder
React Js 16 web application using Functional hooks components. Jest with Enzyme is used for testing.

can be viewed here: https://sherif-personal.gitlab.io/rick-morty-character-episode-finder

## Installation

### Prerequisite
- node
- npm

### Installing dependencies
```bash
npm install
```

## Usage

### Start Development server
```
npm start # run a webpack dev server for application
```
### Run Test
```
npm run test # starts jest watch
```

### Build development
```
npm run build # build dist folder
```

