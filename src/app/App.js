import React, { useState, useEffect } from "react";

//App Global configs
import globalConfigs from "./globalConfigs";

//Components
import Header from "./components/Header/Header";
import Search from "./components/Search/Search";
import CharacterDetails from "./components/CharacterDetails/CharacterDetails";

//Import Routing plugin/library
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CharactersList from "./components/CharactersList/CharactersList";

//Import Axios Http client
import axios from "axios";


function App () {

    const [loading, setLoading] = useState(true);
    const [errorMsg, setErrorMsg] = useState(null);
    const [showLoadMore, setShowLoadMore] = useState(false);

    const [page, setPage] = useState(1);
    const [characters, setCharacters] = useState([]);
    const [characterName, setcharacterName] = useState("");

    useEffect(() => {
        getAllCharactersApi();
    }, [page, characterName]);


    const getAllCharactersApi = () => {

        const apiUrl = globalConfigs.apiDomain + globalConfigs.apiGetAllCharacters + "?page=" + page + "&name=" + characterName;

        axios.get(apiUrl).then(
            (response) => {
                let data = response.data;

                setLoading(false);
                setErrorMsg(null);

                if (data.results) {
                    (page == 1) ? setCharacters(prevCharacters => prevCharacters = data.results) : setCharacters(prevCharacters => prevCharacters.concat(data.results));

                    //To show/hide Loadmore based on pages
                    (data.info.pages > page) ? setShowLoadMore(true) : setShowLoadMore(false);
                } else {
                    setErrorMsg(data.error);
                }

            }).catch((error) => {
                console.log("error: ", error);
                setErrorMsg(error)
            });

    }

    const search = (searchValue) => {
        setPage(1);
        setcharacterName(searchValue);
    }

    const loadMoreCharacters = () => {
        setPage(prevPage => prevPage + 1);
    }

    return (
        <div className="container-fluid">
            <Router basename="/rick-morty-character-episode-finder">
                <Header />
                <Route exact path="/" render={(props) => <Search {...props} search={search} />} />

                {
                    loading && !errorMsg ? (<span>Loading ...</span>) : errorMsg ? (<div className="error">{errorMsg}</div>)
                        : <Route exact path="/" render={(props) => <CharactersList {...props} characters={characters} showLoadMore={showLoadMore} loadMoreCharacters={loadMoreCharacters} />} />
                }

                <Route path="/character/:id" render={(props) => <CharacterDetails {...props} />} />
            </Router>
        </div>
    )
}


export default App;