//Application styles loaded through main scss
import "../style/main.scss";

//Application main entry point
import App from "./App";

import React from "react";
import ReactDom from "react-dom";

//Load and render React APP to the html root element
ReactDom.render(<App/>, document.getElementById("root"));

//For webpack to allow react components to reload without the loss of state in the dev run
module.hot.accept();
