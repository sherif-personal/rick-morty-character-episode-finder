import React from "react";
import ReactDom from "react-dom";
import App from "./App";

import Enzyme, { shallow, render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });


//Test if Header renders correctly
it("APP main wrapper renders correctly", ()=> {
    const wrapper = mount(<App />);
    expect(toJson(wrapper)).toMatchSnapshot();
});