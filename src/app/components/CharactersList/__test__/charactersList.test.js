import React from "react";
import ReactDom from "react-dom";
import CharactersList from "../CharactersList";

import Enzyme, { shallow, render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });


//Test if Header renders correctly
it("Characters List renders correctly", ()=> {
    const wrapper = mount(<CharactersList />);
    expect(toJson(wrapper)).toMatchSnapshot();
});