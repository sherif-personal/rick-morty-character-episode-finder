import React, {useRef, useEffect} from "react";

//Components
import CharacterCard from "../CharacterCard/CharacterCard";


//Import bootstrap components used
import {Button} from "react-bootstrap";

//General scroll to component
const scrollToComponent = (componentRef) => window.scrollTo({top: componentRef.current.offsetTop, behavior: 'smooth'})

const CharactersList = (props) => {
    
    const loadMoreAnchorRef = useRef(null)

    useEffect(()=>{
        scrollToComponent(loadMoreAnchorRef);
    },[props.characters])

    const loadMoreCharacters = (e) => {
        e.preventDefault();
        props.loadMoreCharacters();
    }

    return(

        <div className="container-fluid">
            <div className="row no-gutters">
                <div className="col-12">
                <div className="search-result-wrapper">
                    <div className="characters-cards-wrapper card-group">
                        {props.characters.map(character => <CharacterCard key={character.id} character={character} />)}
                    </div>
                    <div ref={loadMoreAnchorRef}  className="row mt-3">
                        <div className="col">
                            <div className="load-more">
                                {(props.showLoadMore) ? (<Button variant="primary" onClick={loadMoreCharacters}>Load more</Button>) : ""}
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

    )
}

export default CharactersList;