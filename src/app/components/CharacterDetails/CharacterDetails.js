import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

//App Global configs
import globalConfigs from "../../globalConfigs";

//components
import EpisodesList from "../EpisodesList/EpisodesList";
import CharacterCard from "../CharacterCard/CharacterCard";
import { Button } from "react-bootstrap";

//Import Axios Http client
import axios from "axios";

const CharacterDetails = (props) => {

    const [loading, setLoading] = useState(true);

    const [page, setPage] = useState(1);
    const [showLoadMore, setShowLoadMore] = useState(false);

    const [character, setCharacter] = useState();
    const [episodes, setEpisodes] = useState();
    const [relatedCharacters, setRelatedCharacters] = useState();

    useEffect(() => {
        getCharacter(props.match.params.id);
    }, [props.match.params.id]);

    useEffect(() => {
        if (character) {
            getEpisodes(character.episode);
            getRelatedCharacters(character.species);
        }
    }, [character]);

    useEffect(() => {
        if (character) {
            getRelatedCharacters(character.species)
        }
    }, [page]);


    const getCharacter = (id) => {
        const apiUrl = globalConfigs.apiDomain + globalConfigs.apiGetAllCharacters + id;

        axios.get(apiUrl).then(
            (response) => {
                setCharacter(response.data);
                setLoading(false);
            }
        ).catch(
            (error) => {
                console.log("error: ", error);
            }
        )
    }

    const getEpisodes = (episodesList) => {
        let episodesNumberString = "";
        episodesList.filter((episode) => { episodesNumberString = episodesNumberString + (episode.split("episode/"))[1] + "," });

        const apiUrl = globalConfigs.apiDomain + globalConfigs.apiGetAllEpisodes + episodesNumberString;

        axios.get(apiUrl).then(
            (response) => {
                if (response.data.length > 0) {
                    setEpisodes(response.data);
                }
            }
        ).catch(
            (error) => {
                console.log("error: ", error);
            }
        )

    }

    const getRelatedCharacters = (species) => {
        const apiUrl = globalConfigs.apiDomain + globalConfigs.apiGetAllCharacters + "?page=" + page + "&species=" + species;

        axios.get(apiUrl).then(
            (response) => {
                let data = response.data;
                if (data.results) {
                    (page == 1) ? setRelatedCharacters(prevCharacters => prevCharacters = data.results) : setRelatedCharacters(prevCharacters => prevCharacters.concat(data.results));
                }

                //To show/hide Loadmore based on pages
                (data.info.pages > page) ? setShowLoadMore(true) : setShowLoadMore(false);
            }
        ).catch(
            (error) => {
                console.log("error: ", error);
            }
        )
    }

    const loadMoreCharacters = () => {
        setPage(prevPage => prevPage + 1);
    }

    return (
        <div className="container-fluid m-3 mt-4">
            {(loading) ? (<span className="loading">Loading...</span>) :
                (
                    <div data-testid="characterDetailsContainer" className="row no-gutters">
                        <div className="col-6">
                            <div className="character-details-wrapper">

                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-5">
                                            <img src={character.image} />
                                        </div>
                                        <div className="col-7">
                                            <h4 data-testid="characterName">{character.name}</h4>
                                            <p className="m-0">species: {character.species}</p>
                                            <p className="m-0">origin: {character.origin.name}</p>
                                            <p className="m-0">status: {character.status}</p>
                                            <p className="m-0">gender: {character.gender}</p>
                                            <p className="m-0">location: {character.location.name}</p>
                                            <p className="m-0">type: {(character.type) ? character.type : "N/A"}</p>
                                            <div className="d-block mt-2"><Link to="/" className="btn btn-primary font-12"> {"< Back"} </Link></div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12 mt-3">
                                            <EpisodesList episodes={episodes} />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-6">

                            <h4>Characters of similar species:</h4>
                            <div className="characters-cards-wrapper related-characters">
                                {
                                    (relatedCharacters) ? relatedCharacters.filter((relatedCharacter) => relatedCharacter.id != character.id)
                                        .map(character => <CharacterCard key={character.id} character={character} />)
                                        : <span>Loading...</span>
                                }

                                <div className="load-more"> {(showLoadMore) ? (<Button onClick={loadMoreCharacters}>Load more</Button>) : ""} </div>
                            </div>

                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default CharacterDetails;