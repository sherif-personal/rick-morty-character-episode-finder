import React from 'react';
import ReactDOM from 'react-dom';
import CharacterDetails from '../CharacterDetails';
import {act, render, fireEvent, cleanup, waitForElement} from '@testing-library/react';

import axiosMock from "axios";

afterEach(cleanup)

it('Async axios request works', async () => {
  axiosMock.get.mockResolvedValue({data: {
    "id": 2,
    "name": "Morty Smith",
    "status": "Alive",
    "species": "Human",
    "type": "",
    "gender": "Male",
    "origin": {
      "name": "Earth",
      "url": "https://rickandmortyapi.com/api/location/1"
    },
    "location": {
      "name": "Earth",
      "url": "https://rickandmortyapi.com/api/location/20"
    },
    "image": "https://rickandmortyapi.com/api/character/avatar/2.jpeg",
    "episode": [
      "https://rickandmortyapi.com/api/episode/1",
      "https://rickandmortyapi.com/api/episode/2",
      // ...
    ],
    "url": "https://rickandmortyapi.com/api/character/2",
    "created": "2017-11-04T18:50:21.651Z"
  } })

  const { getByText, getByTestId, rerender } = render(<CharacterDetails match={{params: {id: 2}, isExact: true, path: "", url: ""}} />);

  expect(getByText(/Loading.../i).textContent).toBe("Loading...")

  const resolvedEl = await waitForElement(() => getByTestId("characterName"));

  expect((resolvedEl).textContent).toBe("Morty Smith")

  expect(axiosMock.get).toHaveBeenCalledTimes(3);
 })


