import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const CharacterCard = (props) => {

    const [character, setCharacter] = useState();

    useEffect(() => {
        if (props.character) {
            setCharacter(props.character)
        }
    }, [props.character])

    return (
        (!character) ? <span>...Loading</span> :
            <div className="character-card mb-3">
                <Link to={`/character/${character.id}`}>
                    <div>
                        <img src={character.image} />
                    </div>
                    <div className="character-info-wrapper p-1">
                        <div className="row no-gutters">
                            <div className="col">
                                <h6 className="m-0 font-14">{character.name}</h6>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col text-right">
                                <small>episodes &gt; </small>
                            </div>
                        </div>
                    </div>
                </Link>
            </div>
    )
}

export default CharacterCard;