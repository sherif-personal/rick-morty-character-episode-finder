import React from "react";

const EpisodesList = (props) => {

    return(
        <div className="episodes-list-wrapper">
            <h4>Episodes appeard:</h4>
                                                    
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">episode</th>
                        <th scope="col">name</th>
                        <th scope="col">air date</th>
                    </tr>                                        
                </thead>
                <tbody>
                    { (props.episodes) ?  props.episodes.map((episode)=>{ return (
                                                                        <tr key={episode.id}>
                                                                            <td>{episode.episode}</td>
                                                                            <td>{episode.name}</td>
                                                                            <td>{episode.air_date}</td>
                                                                        </tr>
                                                                    )}) : 
                                                                    <tr><td>No episodes found</td></tr> }
                </tbody>
            </table>
        </div>
    )
}

export default EpisodesList;