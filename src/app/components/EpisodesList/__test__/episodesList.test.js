import React from "react";
import ReactDom from "react-dom";
import EpisodesList from "../EpisodesList"

import Enzyme, { shallow, render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });


//Test if Header renders correctly
it("Header renders correctly", ()=> {
    const wrapper = mount(<EpisodesList />);
    expect(toJson(wrapper)).toMatchSnapshot();
});