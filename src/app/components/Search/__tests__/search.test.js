import React from 'react';
import ReactDOM from 'react-dom';

import Search from '../Search';
import {render, fireEvent, cleanup, act} from "@testing-library/react";
import App from "../../../App";

//Cleanup after test
afterEach(cleanup);

it("Input text in search updates state", ()=>{

    const {getByText, getByLabelText} = render(<Search/>);
    
   // expect(getByLabelText("searchInput"))

});

it("Search Button click updates props", ()=>{
    const {getByText} = render(<App>
                                <Search />
                              </App>);

    act( () => {
        fireEvent.click(getByText("Search"));
    })
});