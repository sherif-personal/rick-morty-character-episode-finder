import React, {useState} from "react";
import { Button } from "react-bootstrap";

const Search = (props) => {

    const [searchValue, setSearchValue] = useState("");

    const handleSearchInputChanges = (e) => {
        setSearchValue(e.target.value)
    }

    const callSearchFunction = (e) => {
        e.preventDefault();
        props.search(searchValue);
    }

    return(
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                <div className="characters-search-wrapper">
                    <form className="search-form">
                        <div className="row no-gutters">
                            <div className="col-8">
                                <div className="form-group">
                                    <input className="form-control" value={searchValue} onChange={handleSearchInputChanges} type="text" placeholder="Character name"/>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="form-group">
                                    <Button onClick={callSearchFunction} type="submit" value="SEARCH" >Search</Button>
                                </div>
                            </div>
                        </div>
                    </form>       
                </div>
                </div>
            </div>
        </div>
    );

}

export default Search;