import React from "react";
import { Link } from "react-router-dom";

//images
import logo from "../../../images/rick-morty-logo.png";

const Header = () => {
    return (
        <div className="header">
            <div className="logo"><Link to="/"><img src={logo} /></Link></div>
            <div className="flex-auto">
                <h1>Character episodes finder</h1>
                <p>Search in which episodes your favourite Rick and Morty characters appeared</p>
            </div>
        </div>
    )
}

export default Header