const appConfig = {
    apiDomain: "https://rickandmortyapi.com/api/",
    apiGetAllCharacters: "character/",
    apiGetAllEpisodes: "episode/"
}

export default appConfig;