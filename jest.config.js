module.exports = {
    "moduleNameMapper": {
        ".+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$": "identity-obj-proxy"
      },
      testEnvironmentOptions: {
        // only string values is supported??
        beforeParse (window) {
          window.document.childNodes.length === 0;
          window.alert = (msg) => { console.log(msg); };
          window.matchMedia = () => ({});
          window.scrollTo = () => { };
        }
      }, 
}